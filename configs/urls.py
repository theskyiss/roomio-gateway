"""roomio URL Configuration

The `urlpatterns` list routes URLs to views.
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from main.views import not_found_error, internal_server_error

handler404 = 'main.views.not_found_error'
handler500 = 'main.views.internal_server_error'

admin.site.site_header = "Roomio Gateway Admin"
admin.site.site_title = "Roomio Gateway Admin"
admin.site.index_title = "Welcome to Roomio Gateway Admin"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('_nested_admin/', include('nested_admin.urls')),
    path('', include('main.urls', namespace='main')),
]
