# Roomio Gateway

Current version 0.2, build 24.03.2024

The Roomio Gateway is an open-source solution that allows you to integrate devices connected to legacy and third-party systems with hoteza app.

## Instructions for Installing Docker and Docker Compose on Ubuntu

This guide provides step-by-step instructions for installing Docker and Docker Compose on a server running the Ubuntu operating system.

### Step 1: Installing curl

1. Open the terminal on your server.
2. Execute the following commands to install curl:

        sudo apt-get update
    sudo apt-get install -y curl
    

### Step 2: Installing Docker

1. Execute the following commands to install Docker:

        curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    

2. Add the current user to the Docker group:

        sudo groupadd docker
    sudo usermod -aG docker $USER
    

3. Activate the changes to groups:

        newgrp docker
    

### Step 3: Installing Docker Compose
1. Execute the following commands to install Docker Compose:

    ```bash
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    ```

**Congratulations!** Docker and Docker Compose are now successfully installed on your Ubuntu server. You can proceed with configuring and running your Docker containers using Docker Compose.


### Step 4: Creating an .env file

Create in your parent directory .env file with following context and replace all needed values to your own:

    SECRET_KEY=SECRET_KEY
    DEBUG=False
    ALLOWED_HOSTS=ALLOWED_HOSTS
    HOST=0.0.0.0
    PORT=1880
    DB_NAME=DB_NAME
    DB_USER=DB_USER
    DB_PASS=DB_PASS
    DB_HOST=postgres
    DB_PORT=5432
    

### Step 5: Running a Docker container
1. Copy `docker-compose.yml` file into the desired directory on your server.

2. Navigate to the directory containing your `docker-compose.yml` file:

    ```bash
    cd /path/to/your/docker-compose-directory
    ```
   Optional: if there would be errors with entrypoint.sh file, run this command with proper path to your entrypoint.sh:

    ```bash
    chmod +x your/path/to/entrypoint.sh
    ```

3. Run the following command to start your Docker containers:

    ```bash
    docker-compose -f docker-compose.yml up -d
    ```
### Step 6: Create admin user

1. To create admin user to configure settings, run:
   ```bash
    docker exec -it roomio_app python manage.py createsuperuser
    ```

2. Create login and password using the command output

### Result: 
So, now you are able to configure your own server settings. You can go to YOURHOST/admin and log in with credentials you have created in your 6 step. In admin site can be found server settings in tab Servers and Device templates with preinstalled data, which you can choose to your settings.
