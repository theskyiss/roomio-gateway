#!/bin/sh

python manage.py migrate
python manage.py loaddata fixtures/*
python manage.py collectstatic --no-input
python manage.py runserver 0.0.0.0:1880
exec "$@"
