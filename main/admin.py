from django.contrib import admin
from nested_admin import NestedTabularInline, NestedModelAdmin
from .models import Server, Device, Scene, DeviceLocation, Tag, TagsAndValue


class TagAdminInline(NestedTabularInline):
    model = Tag
    extra = 3


class TagsAndValueAdminInline(NestedTabularInline):
    model = TagsAndValue
    extra = 0


class SceneAdminInline(NestedTabularInline):
    model = Scene
    inlines = [TagsAndValueAdminInline]
    extra = 0


class DeviceLocationAdminInline(NestedTabularInline):
    model = DeviceLocation
    inlines = [TagAdminInline]
    extra = 0


class DeviceAdminInline(NestedTabularInline):
    model = Device
    inlines = [DeviceLocationAdminInline, SceneAdminInline]
    extra = 0


class ServerAdmin(NestedModelAdmin):
    inlines = [DeviceAdminInline]


admin.site.register(Server, ServerAdmin)
