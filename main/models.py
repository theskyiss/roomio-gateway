from django.db import models
from .choices import LOGGING_LEVEL


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


class Server(SingletonModel):
    name = models.CharField(max_length=200, default='Roomio Server')
    method = models.CharField(max_length=50, default='tcp')
    app_host = models.GenericIPAddressField(default='0.0.0.0')
    app_port = models.IntegerField(default=1880)
    timeout = models.IntegerField(default=2)
    max_log_days = models.IntegerField(default=3)
    logging = models.CharField(max_length=50, choices=LOGGING_LEVEL, default=LOGGING_LEVEL[0][0])
    check_modbus_interval = models.IntegerField(default=60)
    check_modbus_interval_after_failed_attempt = models.IntegerField(default=120)


class Device(models.Model):
    device_name = models.CharField(max_length=200)
    hrc_ip = models.GenericIPAddressField()
    hrc_port = models.IntegerField()
    unit_id = models.IntegerField()
    device_template_id = models.IntegerField()
    room = models.IntegerField()
    server = models.ForeignKey(Server, on_delete=models.CASCADE, related_name='devices')


class Scene(models.Model):
    name = models.CharField(max_length=200)
    device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='scenes')


class DeviceLocation(models.Model):
    device_location = models.CharField(max_length=200)
    device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='device_locations')


class Tag(models.Model):
    modbus_id = models.IntegerField()
    device_location = models.ForeignKey(DeviceLocation, on_delete=models.CASCADE, related_name='tags')


class TagsAndValue(models.Model):
    modbus_id = models.IntegerField()
    value = models.IntegerField()
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE, related_name='tags_and_values')
