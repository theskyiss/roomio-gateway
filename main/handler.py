from .constants import *
from .room_modbus import RoomModbus
from .models import Server
from .choices import CURTAIN_STATE
from device_templates.models import DeviceTemplate
from configs.settings import WRITE_COMMANDS_TUPLE, READ_COMMANDS_TUPLE

import time
import logging

logger = logging.getLogger(__name__)


class Handler:
    def __init__(self):
        self.connected_clients = []

    @property
    def server(self):
        return self.get_config()

    @property
    def templates(self):
        return self.get_device_templates()

    @staticmethod
    def get_config():
        logger.info("Loading config")
        return Server.load()

    @staticmethod
    def get_device_templates():
        return DeviceTemplate.objects.all()

    def start(self):
        self.check_connection()

    def get_client_for_connect(self, device):
        modbus_host = getattr(device, 'hrc_ip', '127.0.0.1')
        modbus_port = getattr(device, 'hrc_port', 502)
        modbus_name = getattr(device, 'device_name', 'deviceName')
        modbus_timeout = getattr(device, 'timeout', 2)
        client = RoomModbus(modbus_host, modbus_port, timeout=modbus_timeout)
        client.tags = self.templates.get(
            device_template_id=device.device_template_id).tags.all()

        for location in device.device_locations.all():
            for location_tag in location.tags.all():
                for client_tag in client.tags:
                    if client_tag.modbus_id == location_tag.modbus_id:
                        client_tag.device_location = location.device_location
        logger.debug(f"Loaded device: {modbus_name}: {modbus_host}:{modbus_port}")
        return client

    def get_client(self, device):
        if self.connected_clients:
            for client in self.connected_clients:
                if client and client.tcp_client.params.host == device.hrc_ip and \
                        client.tcp_client.params.port == device.hrc_port:
                    return client
        # logger.error(f"Failed to connect to Modbus server: {device.hrc_ip}:{device.hrc_port}")
        raise Exception(f"Failed to connect to Modbus server: {device.hrc_ip}:{device.hrc_port}")

    def check_connection(self):
        # For checking modbus connection we will try to read 1 tag
        wait_after_failed_attempt = getattr(self.server, 'check_modbus_interval_after_failed_attempt', 0)
        if wait_after_failed_attempt < 4:
            wait_after_failed_attempt = 4
        current_time = time.time()
        for device in self.server.devices.all():
            client = self.get_client_for_connect(device)
            if client.next_check_time < current_time:
                # For check, we will read first tag in tags dict
                tag_for_check = next(iter(client.tags))
                try:
                    RoomModbus.read_modbus_data(client.tcp_client, tag_for_check.address, tag_for_check.type,
                                                device.unit_id, readForTesting=True)
                    client.next_check_time = current_time + self.server.check_modbus_interval
                    self.connected_clients.append(client)
                except Exception as e:
                    client.next_check_time = current_time + self.server.check_modbus_interval_after_failed_attempt
                    logger.error(f"An error occurred while testing the device connection: {str(e)}")

    # removed threads
    # def starting_thread(self):
    #     check_connection_thread = Thread(target=self.check_connection, daemon=True)
    #     check_connection_thread.start()

    def handle_state_request(self, req_type, room, params):
        device_found = False
        result = {}
        for device in self.server.devices.all():
            client = self.get_client(device)
            if device.room == room:
                if req_type == FCU_PARAMETR:
                    device_found, result = fcu_handle_state(client, device, req_type, params, room)
                else:
                    for tag in client.tags:
                        if tag.device_type == req_type and params['deviceLocation'] == tag.device_location:
                            if tag.function_code_read in READ_COMMANDS_TUPLE:
                                result["state"] = RoomModbus.read_modbus_data(client.tcp_client, tag.address, tag.type,
                                                                              device.unit_id)
                                device_found = True
        return device_found, result

    def handle_push_request(self, req_type, room, params, state):
        device_found = False
        if req_type == 'scene':
            scene_name = params['deviceLocation']
            device_found, x = self.scene_handle_push(scene_name, room)
        else:
            for device in self.server.devices.all():
                client = self.get_client(device)
                if room and device.room == room:
                    for tag in client.tags:
                        if (tag.device_type == req_type or
                            tag.device_type == 'light' and req_type == DIMMER_PARAMETR) and \
                                params['deviceLocation'] == tag.device_location:
                            if tag.function_code_write in WRITE_COMMANDS_TUPLE:
                                if tag.device_type == FCU_PARAMETR:
                                    fcu_handle_push(client.tcp_client, tag.address, tag.type, device.unit_id,
                                                    tag.tag_name, params)
                                elif tag.device_type == DIMMER_PARAMETR:
                                    light_level = params.get('level', None)
                                    value = light_level if light_level is not None else state
                                    RoomModbus.write_modbus_data(client.tcp_client, tag.address, value, tag.type,
                                                                 device.unit_id)
                                elif tag.device_type == CURTAIN_PAUSE_PARAMETR:
                                    if state.lower() in CURTAIN_STATE:
                                        state = CURTAIN_STATE[state.lower()]
                                    else:
                                        raise ValueError("Wrong state parameter")
                                    RoomModbus.write_modbus_data(client.tcp_client, tag.address, state, tag.type,
                                                                 device.unit_id)
                                else:
                                    RoomModbus.write_modbus_data(client.tcp_client, tag.address, state, tag.type,
                                                                 device.unit_id)
                                device_found = True
        return device_found

    def scene_handle_push(self, scene_name, scene_room_from_request):
        scene_found = False
        for device in self.server.devices.all():
            client = self.get_client(device)
            if device.room == scene_room_from_request or scene_room_from_request is None:
                for scene in device.scenes.all():
                    if scene.name == scene_name:
                        for tag_and_value in scene.tags_and_values.all():
                            device_type = getattr(tag_and_value, 'device_type', None)
                            device_location = getattr(tag_and_value, 'device_location', None)
                            modbus_id = getattr(tag_and_value, 'modbus_id', None)
                            for tag in client.tags:
                                if (device_type is None or tag.device_type == device_type) and \
                                        (modbus_id is None or tag.modbus_id == modbus_id) and \
                                        (device_location is None or tag.device_location == device_location):
                                    if tag.function_code_write in WRITE_COMMANDS_TUPLE:
                                        RoomModbus.write_modbus_data(client.tcp_client, tag.address,
                                                                     tag_and_value.value, tag.type, device.unit_id)
                                        scene_found = True
        return scene_found, 0


def fcu_handle_push(client, address, tag_type, unit_id, tag_name, params):
    value = params[tag_name]
    if value is not None:
        RoomModbus.write_modbus_data(client.tcp_client, address, value, tag_type, unit_id)


def fcu_handle_state(client, device, req_type, params, room):
    result = {}
    tag_names = ['fan', 'mode', 'temperature', 'curtemperature']
    if device.room == room:
        for tag in client.tags:
            if tag.device_type == req_type and params.get('device_location') == tag.device_location:
                if tag.tag_name in tag_names and tag.function_code_read in READ_COMMANDS_TUPLE:
                    result[tag.tag_name] = RoomModbus.read_modbus_data(client.tcp_client, tag.address, tag.type,
                                                                       device.unit_id)
        device_found = True if result else False
        return device_found, result
