from django.urls import path
from .views import SignUpView, SignInView, EditorView, PushRequest, StateRequest

app_name = 'main'

urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('login/', SignInView.as_view(), name='login'),
    path('editor/', EditorView.as_view(), name='editor'),
    path('push.php/', PushRequest.as_view(), name='push'),
    path('state.php/', StateRequest.as_view(), name='state'),
]
