import logging
import atexit
from django.apps import AppConfig
from configs.settings import HOST, PORT

logger = logging.getLogger(__name__)

class MainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'

    def ready(self):
        logger.info(f"Starting Django at {HOST}:{PORT}")


def log_shutdown_message():
    logger.info("RoomioGateway stopped")


atexit.register(log_shutdown_message)
