LOGGING_LEVEL = (
    ("INFO", "INFO"),
    ("DEBUG", "DEBUG"),
)

CURTAIN_STATE = {
    "open": 1,
    "close": 0,
    "stop": 2,
}