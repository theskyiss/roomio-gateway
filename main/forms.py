from django import forms
from django.forms import inlineformset_factory
from .models import Server, Device, DeviceLocation, Scene, Tag, TagsAndValue


class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        fields = ['name', 'app_host', 'app_port', 'timeout', 'logging', 'check_modbus_interval',
                  'check_modbus_interval_after_failed_attempt']


DeviceFormSet = inlineformset_factory(
    Server,
    Device,
    fields=('device_name', 'hrc_ip', 'hrc_port', 'unit_id', 'device_template_id', 'room'),
    extra=0,
    can_delete=True
)

DeviceLocationFormSet = inlineformset_factory(
    Device,
    DeviceLocation,
    fields=('device_location',),
    extra=0,
    can_delete=True
)

SceneFormSet = inlineformset_factory(
    Device,
    Scene,
    fields=('name',),
    extra=0,
    can_delete=True
)

TagFormSet = inlineformset_factory(
    DeviceLocation,
    Tag,
    fields=('modbus_id',),
    extra=0,
    can_delete=True
)

TagsAndValueFormSet = inlineformset_factory(
    Scene,
    TagsAndValue,
    fields=('modbus_id', 'value'),
    extra=0,
    can_delete=True
)
