from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.views.generic.edit import FormView
from django.views.generic import CreateView
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse_lazy
from django.http import JsonResponse, HttpResponse

import json
import logging
from urllib.parse import unquote

from .constants import *
from .handler import Handler
from .forms import (
    ServerForm,
    DeviceFormSet,
    DeviceLocationFormSet,
    SceneFormSet,
    TagFormSet,
    TagsAndValueFormSet
)

logger = logging.getLogger(__name__)

@csrf_exempt
def not_found_error(request, exception):
    logger.warning(f"404 - Not Found. The requested URL was not found on the server.\n "
                   f"Request from IP: {request.META['REMOTE_ADDR']}, URL: {request.build_absolute_uri()}")
    return JsonResponse({"message": "404 - Not Found"}, status=404)


@csrf_exempt
def internal_server_error(request):
    logger.warning(f"500 - internal server error.\n "
                   f"Request from IP: {request.META['REMOTE_ADDR']}, URL: {request.build_absolute_uri()}")
    return JsonResponse({"message": "500 - internal server error"}, status=500)


class SignUpView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class SignInView(LoginView):
    form_class = AuthenticationForm
    template_name = 'login.html'


class StateRequest(View):
    login_url = '/login/'
    redirect_field_name = '/editor/'

    def get(self, request):
        try:
            handler = Handler()
            handler.start()
            ip = request.META.get('REMOTE_ADDR', 'IP not available')
            logger.debug(f"Request from IP: {ip}, URL: {request.build_absolute_uri()}")
            req_type = request.GET.get('type')
            if req_type == 'dimmer':
                req_type = 'light'
            room = int(request.GET.get('room'))
            params_encoded = request.GET.get('params')
            params_decoded = unquote(unquote(params_encoded))
            params = json.loads(params_decoded)
            device_found, result = handler.handle_state_request(req_type, room, params)
            if device_found:
                logger.debug(f"Send: {result} to IP: {request.META['REMOTE_ADDR']}")
                return JsonResponse(result)
            else:
                raise ValueError("Device not found")

        except Exception as e:
            # Log the exception or handle it in an appropriate way
            logger.error(f"An error occurred: {str(e)}")
            logger.debug(f"Send: message: {str(e)} to IP: {request.META['REMOTE_ADDR']}")
            return JsonResponse({"message": str(e)}, status=500)


class PushRequest(View):
    login_url = '/login/'
    redirect_field_name = '/editor/'

    def get(self, request):
        try:
            handler = Handler()
            handler.start()
            ip = request.META.get('REMOTE_ADDR', 'IP not available')
            logger.debug(f"Request from IP: {ip}, URL: {request.build_absolute_uri()}")
            kwargs = request.GET.dict()
            req_type = kwargs.get('type')
            params_encoded = kwargs.get('params')
            params_decoded = unquote(unquote(params_encoded))
            params = json.loads(params_decoded)
            room = int(kwargs.get(ROOM_PARAMETR)) if ROOM_PARAMETR else None
            state = kwargs.get('state', None)
            device_found = handler.handle_push_request(req_type, room, params, state)
            if device_found:
                logger.debug(f"Send: 'OK' to IP: {request.META['REMOTE_ADDR']}")
                return HttpResponse('OK', content_type="text/plain; charset=utf-8")
            else:
                raise ValueError("Device not found")

        except Exception as e:
            # Log the exception or handle it in an appropriate way
            logger.error(f"An error occurred: {str(e)}")
            logger.debug(f"Send: message: {str(e)} to IP: {request.META['REMOTE_ADDR']}")
            return JsonResponse({"message": str(e)}, status=500)


class EditorView(LoginRequiredMixin, FormView):
    login_url = '/login/'
    redirect_field_name = '/editor/'
    template_name = 'editor.html'
    form_class = ServerForm

    def get_context_data(self, **kwargs):
        data = super(EditorView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['device_formset'] = DeviceFormSet(self.request.POST)
            data['device_location_formset'] = DeviceLocationFormSet(self.request.POST)
            data['scene_formset'] = SceneFormSet(self.request.POST)
            data['tag_formset'] = TagFormSet(self.request.POST)
            data['tags_and_value_formset'] = TagsAndValueFormSet(self.request.POST)
        else:
            data['device_formset'] = DeviceFormSet()
            data['device_location_formset'] = DeviceLocationFormSet()
            data['scene_formset'] = SceneFormSet()
            data['tag_formset'] = TagFormSet()
            data['tags_and_value_formset'] = TagsAndValueFormSet()
        return data

    def form_valid(self, form):
        try:
            context = self.get_context_data()
            device_formset = context.get('device_formset')
            device_location_formset = context.get('device_location_formset')
            scene_formset = context.get('scene_formset')
            tag_formset = context.get('tag_formset')
            tags_and_value_formset = context.get('tags_and_value_formset')

            formsets = [device_formset, device_location_formset, scene_formset, tag_formset, tags_and_value_formset]

            if all(formset.is_valid() if formset else True for formset in formsets):
                self.object = form.save()

                for formset in formsets:
                    formset.instance = self.object
                    formset.save()

                return super(EditorView, self).form_valid(form)
            else:
                # for formset in formsets:
                #     if not formset.is_valid():
                #         print(f'Errors in {formset}: {formset.errors}')
                return self.form_invalid(form)
        except BaseException as e:
            raise e
