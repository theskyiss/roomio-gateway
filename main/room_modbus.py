import pymodbus
from pymodbus.client import ModbusTcpClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
import logging

# off pymodbus logger
pymodbus.logging.pymodbus_apply_logging_config(50)

logger = logging.getLogger(__name__)


class RoomModbus:
    def __init__(self, host, port, timeout=10, next_check_time=0, tags=[]):
        self.tcp_client = ModbusTcpClient(host, port, retries=2, retry_on_empty=True, timeout=timeout)
        self.next_check_time = next_check_time
        self.tags = tags
        self.daemon = True

    # Function to read data from Modbus device
    @staticmethod
    def read_modbus_data(client, register_address, data_type, unit_id, functionCodeRead=3, readForTesting=False):
        # Connect to the Modbus server
        if not client.is_socket_open():
            logger.debug(f"Connecting to modbus server: {client.params.host}:{client.params.port}")
            if not client.connect():
                raise Exception(f"Failed to connect to Modbus server: {client.params.host}:{client.params.port}")

        # Define the appropriate data type for Modbus request
        # TODO add another types
        registrCount = 2
        if data_type.lower() == 'float':
            data_type = 'float32'
        elif data_type.lower() == 'holdingregister':
            data_type = 'holdingRegister'
        else:
            raise ValueError("Invalid data type. Please use 'float' or 'holdingRegister'")
        # TODO: We assume that all tags have function code 3, we should rewrite this part if we would like use another cods
        # Read holding register based on data type
        if not readForTesting:
            logger.debug(f"Reading from modbus server: {client.params.host}:{client.params.port} \n"
                         f"Start address: {register_address}, registers count: {registrCount}")

        # TODO: We assume that all tags have function code 3, we should rewrite this part if we would like use another cods

        response = client.read_holding_registers(register_address, registrCount, slave=unit_id)

        #return 123
        # Check if the response is valid
        if not response.isError():
            decoder = pymodbus.payload.BinaryPayloadDecoder.fromRegisters(response.registers, byteorder=Endian.Big)

            if not readForTesting:
                logger.debug(f"Read data from modbus server:: {client.params.host}:{client.params.port} \n"
                             f"address: {register_address}, payload: {decoder._payload}")
            if data_type == 'float32':
                result = decoder.decode_32bit_float()
            elif data_type.lower() == 'holdingregister':
                result = decoder.decode_16bit_uint()
            else:
                raise ValueError("Invalid data type")

            return result
        else:
            raise Exception(response)
            # TODO : we should try to reconnect if 10053 error

    # Function to write data to Modbus device
    @staticmethod
    def write_modbus_data(client, register_address, data_to_write, data_type, unit_id, functionCodeWrite=6):
        # Connect to the Modbus server
        if not client.is_socket_open():
            logger.debug(f"Connecting to modbus server: {client.params.host}:{client.params.port}")
            if not client.connect():
                raise Exception(f"Failed to connect to Modbus server: {client.params.host}:{client.params.port}")

        builder = BinaryPayloadBuilder(byteorder=Endian.Big)

        # Define the data type for writing
        if data_type.lower() == 'float':
            data_to_write = float(data_to_write)
            builder.add_32bit_float(data_to_write)
        elif data_type.lower() == 'holdingregister':
            data_to_write = int(data_to_write)
            builder.add_16bit_uint(data_to_write)
        else:
            raise ValueError("Invalid data type. Please use 'float' or 'holdingRegister'")

        payload = builder.build()
        # TODO: We assume that all tags have function code 6, we should rewrite this part if we would like use another cods
        # Write to the holding register
        logger.debug(f"Writing to modbus server: {client.params.host}:{client.params.port} \n"
                     f"address: {register_address}, payload: {payload}")
        response = client.write_registers(register_address, payload, slave=unit_id, skip_encode=True)
        if not response.isError():
            logger.debug(f"Successfully wrote to modbus server: {client.params.host}:{client.params.port} \n"
                         f"address: {register_address}, data: {data_to_write} ")
        else:
            raise Exception(response)
