$(document).ready(function () {
    $('#device_formset .dynamic-form').formset({
        prefix: 'device_formset',
        addText: 'add device',
        deleteText: 'remove device',
        addCssClass: 'add-row',
        deleteCssClass: 'delete-row',
        formCssClass: 'dynamic-form',
    });
    $(".add-row").on("click", function(){
        $("#device_formset").css("display", "block");
    })

    let totalForms = document.createElement('input');
    totalForms.setAttribute('type', 'hidden');
    totalForms.setAttribute('name', 'device_formset-TOTAL_FORMS');
    totalForms.setAttribute('value', '1');  // Change this to the actual number of forms
    $(".editor-form").append(totalForms);

    let initialForms = document.createElement('input');
    initialForms.setAttribute('type', 'hidden');
    initialForms.setAttribute('name', 'device_formset-INITIAL_FORMS');
    initialForms.setAttribute('value', '0');  // Change this to the actual number of initial forms
    $(".editor-form").append(initialForms);

    let minNumForms = document.createElement('input');
    minNumForms.setAttribute('type', 'hidden');
    minNumForms.setAttribute('name', 'device_formset-MIN_NUM_FORMS');
    minNumForms.setAttribute('value', '0');  // Change this to the actual minimum number of forms
    $(".editor-form").append(minNumForms);
});
