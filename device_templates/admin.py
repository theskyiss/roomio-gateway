from django.contrib import admin
from .models import DeviceTemplate, DeviceTemplateTag


class DeviceTemplateTagAdminInline(admin.StackedInline):
    model = DeviceTemplateTag
    extra = 0


class DeviceTemplateAdmin(admin.ModelAdmin):
    model = DeviceTemplate
    inlines = [DeviceTemplateTagAdminInline]

admin.site.register(DeviceTemplate, DeviceTemplateAdmin)
