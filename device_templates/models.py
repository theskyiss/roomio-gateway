from django.db import models


class DeviceTemplate(models.Model):
    device_template_name = models.CharField(max_length=255)
    device_template_id = models.IntegerField()


class DeviceTemplateTag(models.Model):
    modbus_id = models.IntegerField()
    tag_name = models.CharField(max_length=255)
    function_code_read = models.IntegerField()
    function_code_write = models.IntegerField()
    type = models.CharField(max_length=255)
    address = models.IntegerField()
    device_location = models.CharField(max_length=255, null=True, blank=True)
    device_type = models.CharField(max_length=255)
    device_template = models.ForeignKey(DeviceTemplate, on_delete=models.CASCADE, related_name='tags')
